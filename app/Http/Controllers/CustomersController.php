<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Customer; 
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreXXXXXXPostRequest;


class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
    
        if (Auth::check()) {
            $id= Auth::id();
            $customers = Customer::all();
            
         
            
            return view('customers.index',['customers'=>$customers],['id'=>$id]);
            }
            return redirect()->intended('/home');

    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $id =Auth::id();
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_id = $id;
        $customer->save();
        return redirect('customers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $customer = Customer::find($id);

        if (Gate::denies('manager')) {
           
            if($customer->user_id != Auth::id()){
                abort(403,"Sorry you do not hold permission to edit this customer");
            }

     
        }
        
        return view('customers.edit', compact('customer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer -> update($request->all());
        return redirect('customers');
    }

    public function supdate($id)
    {   
        if (Gate::denies('manager')) {
            return redirect('customers');
        }
        else{    
        $customer = Customer::find($id);
        $customer->status=1;
        $customer->update();
        return redirect('customers');
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete customers..");
        } 

        $customer->delete();
        return redirect('customers');
    }
}

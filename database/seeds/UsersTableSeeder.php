<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            {
                DB::table('users')->insert(
                [
                    [
                            'name' => 'a_manager',
                            'email' => 'a@a.com',
                            'password' => Hash::make('12345678'),
                            'role' => 'manager',
                            'created_at' => date('Y-m-d G:i:s'),
                    ],
                    [
                        'name' => 'b_sale',
                        'email' => 'b@b.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'salesrep',
                        'created_at' => date('Y-m-d G:i:s'),
                    ],
                    [
                        'name' => 'c_sale',
                        'email' => 'c@c.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'salesrep',
                        'created_at' => date('Y-m-d G:i:s'),
                    ],
                
                ]);
            }
        
        }
    
    }
}

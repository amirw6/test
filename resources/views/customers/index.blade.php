
@extends('layouts.app')

@section('content')

<h1>This is The Customers list</h1>
<table>
  
  <tr>
 
        <a href="{{route('customers.create')}}">Create New customers </a>
    
  </tr>

  <tr>  
    <th>Name</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Customer's create by</th>
    <th> Actions </th>
    <th>status</th>
  </tr>

  
    @foreach($customers as $customer)
    @if ($customer->user_id==$id)
    <tr style="font-weight: bold">
    @else
    <tr>
    @endif
    @if ($customer->status==1)
    {
      <td bgcolor="green" > 
        {{$customer->name}} 
      </td>
    }
    @else{
      <td> 
        {{$customer->name}} 
      </td>
    }
    @endif 
      <td> {{$customer->email}} </td>
      <td> {{$customer->phone}} </td>
      <td> {{$customer->user->name}} </td> 
      <td>   
            <a href="{{route('customers.edit',$customer->id)}}"> Edit </a>
            @cannot('salesrep') <a href="{{route('delete',$customer->id)}}"> @endcannot Delete </a>
            
      </td>

      <td> 
            @if ($customer->status==1)
            
            @else
            @cannot('salesrep')<a href="{{route('supdate',$customer->id)}}">  Deal Closed </a>@endcannot
            
            @endif
      </td>



     </tr>
     @endforeach

</table>
@endsection



@extends('layouts.app')

@section('content')


<h1>Edit  Customer</h1>
<form method = 'post' action="{{action('CustomersController@update', $customer->id)}}">
@csrf
@method('PATCH')


<div class = "form-group">

    <label for = "name">Name of Customer:</label>
    <input type= "text" class = "form-control" name= "name" value = "{{$customer->name}}">
       
</div>
<br> 
<div class = "form-group">
    <label for = "email">Email:</label>
    <input type= "text" class = "form-control" name= "email" value = "{{$customer->email}}">
</div>
<br> 
<div class = "form-group">
    <label for = "phone">Phone:</label>
    <input type= "text" class = "form-control" name= "phone" value = "{{$customer->phone}}">
</div>


<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Update Customer">
</div>

</form>
@endsection
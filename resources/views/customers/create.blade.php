
@extends('layouts.app')

@section('content')


<h1>Create New Customer</h1>
<form method = 'post' action="{{action('CustomersController@store')}}">
{{csrf_field()}}

<div class = "form-group">

    <label for = "name">Name of Customer:</label>
    <input type= "text" class = "form-control" name= "name">
       
</div>
<br> 
<div class = "form-group">
    <label for = "email">Email:</label>
    <input type= "text" class = "form-control" name= "email">
</div>
<br> 
<div class = "form-group">
    <label for = "phone">Phone:</label>
    <input type= "text" class = "form-control" name= "phone">
</div>


<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save New Customer">
</div>

</form>
@endsection